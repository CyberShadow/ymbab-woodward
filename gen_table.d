import std.algorithm.iteration;
import std.array;
import std.conv;
import std.file;
import std.format;
import std.stdio;
import std.string;

import ae.utils.json;

struct Entry
{
	int IndexNumber;
	string EntryType;
	string Headline;
	string FirstLine, SecondLine, ThirdLine;
	string SpriteName;
}

void main()
{
	auto entries = "library.json".readText.jsonParse!(Entry[]);
	auto f = File("10-table.steamguide", "wb");
	f.writeln("Entries");
	f.writeln();
	f.writeln("[table][tr][th]#[/th][th]Title[/th][th]Content[/th][/tr]");
	foreach (entry; entries)
	{
		string title;
		switch (entry.EntryType)
		{
			case "DungeonModifier":
				title = "Woodwardopedia: [b]%s[/b]".format(entry.Headline);
				break;
			case "Monster":
				title = "Monster Analysis: [b]%s[/b]".format(entry.Headline);
				break;
			case "Item":
				title = "Item Analysis: [b]%s[/b]".format(entry.Headline);
				break;
			case "Spell":
				title = "Spell Analysis: [b]%s[/b]".format(entry.Headline);
				break;
			case "Trap":
				title = "Trap Analysis: [b]%s[/b]".format(entry.Headline);
				break;
			case "Enchantment":
				title = "[b]%s[/b]".format(entry.Headline);
				break;
			default:
				throw new Exception(entry.EntryType);
		}

		string content;
		foreach (line; [entry.FirstLine, entry.SecondLine, entry.ThirdLine])
		{
			string tag;
			bool inBold, inTag;
			foreach (c; line)
			{
				assert(c != '[' && c != ']');
				if (inTag)
				{
					if (c == '>')
					{
						inTag = false;
						assert(tag.length == 7 && tag[0] == '#', tag);
						if (tag == "#FFFFFF")
						{
							assert(inBold);
							content ~= "[/b]";
							inBold = false;
						}
						else
						{
							assert(!inBold, entry.text);
							content ~= "[b]";
							inBold = true;
						}
						tag = null;
					}
					else
						tag ~= c;
				}
				else
				{
					if (c == '<')
						inTag = true;
					else
						content ~= c;
				}
			}
			assert(!inTag);
			if (inBold)
				content ~= "[/b]";
			content ~= "\n";
		}

		content = content
			.strip
			.replace(` [/b]`, `[/b] `);
		
		f.writefln("[tr][td]#%03d[/td][td]%s[/td][td]%s[/td][/tr]",
			entry.IndexNumber,
			title,
			content,
		);
	}
	f.writefln("[/table]");
}

//  "